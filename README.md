This is a custom PHP MVC (model-view-controller) framework written for the udemy class by Dave Hollingworth, "Write PHP Like a Pro: Build a PHP MVC Framework From Scratch".

It was built using PHP 7.4.

It makes use of the Twig templating engine from https://twig.symfony.com/

You will need to configure a database connection under App/Config.php.
