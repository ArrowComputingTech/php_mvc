<?php

namespace Core;

/**
 * Base controller
 * PHP v 7.4
 */
abstract class Controller {
  /**
   * Parameters from the matched route
   * @var array
   */
  protected $route_params = [];

  /**
   * Class constructor
   *
   * Parameters from the route
   * @param array  $route_params
   * @return void
   */

  public function __construct($route_params) {
    $this->route_params = $route_params;
  }

  public function __call($name, $args) {
    $method = $name . 'Action';
    if (method_exists($this, $method)) {
      if ($this->before() !== false) {
        call_user_func([$this, $method], $args);
        $this->after();
      }
    } else {
      throw new \Exception("Method $method not found in controller " . get_class($this));
    }
  }

  /**
   * Before filter - call before an action method
   * @return void
   */
  protected function before() {

  }

  /**
   * After filter - call after an action method
   * @return void
   */
  protected function after() {

  }


}
