<?php

namespace App\Controllers;

use \Core\View;

/**
 * Home controller
 * PHP v 7.4
 */
class Home extends \Core\Controller {
  /**
   * Before filter
   * @return void
   */
  protected function before() {
    // Display whatever, if anything...
    // echo "(before)<br>";
  }

  /**
   * After filter
   * @return void
   */
  protected function after() {
    // Display whatever, if anything...
    // echo " (after)<br>"; 
  }

  /**
   * Show the index page
   * @return void
   */
  public function indexAction() {
    // echo "Hello from the index action in the Home controller!<br>";
    /*
    View::render('Home/index.php', [
      'name' => 'Dave',
      'colours' => ['red', 'green', 'blue']
    ]);
     */
    View::renderTemplate('Home/index.html', [
      'name' => 'Dave',
      'colours' => ['red', 'green', 'blue']
    ]);
  }
}
