<?php

namespace App\Controllers\Admin;

/**
 * User admin controller
 * PHP v 7.4
 */

class Users extends \Core\Controller {

  /**
   * Before filter - call before an action method
   * @return void
   */
  protected function before() {

  }

  /**
   * After filter - call after an action method
   * @return void
   */
  public function indexAction() {
    echo "User admin index";
  }
}
