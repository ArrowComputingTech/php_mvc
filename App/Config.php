<?php

  namespace App;
  
  /**
   * Application configuration
   * PHP v 7.4
   */
  class Config {
    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'mvc';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'db_user';

    /**
     * Database host
     * @var string
     */
    const DB_PASSWORD = 'db_password';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = false;
  }
